/**
 * 亞趣網站測試腳本
 * 1.導向網站首頁
 * (1)可在瀏覽器輸入網址後，看見網站首頁
 * 2.登入功能測試
 * (1)可點擊[登入/註冊]按鈕，導向登入頁
 * (2)在登入頁輸入帳號密碼，可登入網站
 * 3.登出功能測試
 * (1)登入後，可在會員頭像處選擇登出
 */

// 初始化webdriverio
// 作為 Appium 的客戶端，連結 Appium Server 與 客戶端作業系統
const webdriverio = require('webdriverio');
// 斷言
// Mocha 的斷言函式庫，用來判斷測試結果是否符合預期
const assert = require("assert");

describe('亞趣遊艇交易平台網頁功能測試', function () {
    // 初始化變數作為客戶端操作
    let client;

    // 在全部的測試之前執行
    before(async function () {
        // 初始化客戶端
        const opts = {
            capabilities: {
                browserName: 'chrome',
                automationName: 'Chromium',
                platformName: "Windows",
            }
        }

        client = await webdriverio.remote(opts);
        // 最大化視窗
        // await client.maximizeWindow();
    });

    describe('1.導向網站首頁', function () {
        it('(1)可在瀏覽器輸入網址後，看見網站首頁', async function () {
            // 訪問網頁
            await client.url('https://buy.yachsp.com/');
            // 確認網頁標題
            const title = await client.getTitle();
            // 斷言
            assert.strictEqual(title, '-亞趣遊艇交易平台');
        });
    });

    describe('2.登入功能測試', function () {
        it('(1)可點擊[登入/註冊]按鈕，導向登入頁', async function () {
            // 將鼠標移動到人像處
            const avatar = await client.$('//*[@id="__nuxt"]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/div[1]');
            await avatar.moveTo();
            // 點擊[登入/註冊]按鈕
            const loginBtn = await client.$('#login');
            await loginBtn.waitForClickable();
            await loginBtn.click();
            client.pause(5000);
            // 確認是否導向登入頁
            await client.waitUntil(async () =>
                await client.getUrl() === 'https://buy.yachsp.com/user/login',
                {timeout: 10000}
            );
            assert.strictEqual(await client.getUrl(), 'https://buy.yachsp.com/user/login');
        });

        it('(2)在登入頁輸入帳號密碼，可登入網站', async function () {
            // 輸入帳號
            const accountInput = await client.$('input[placeholder="Email address"]');
            await accountInput.click();
            await accountInput.setValue('codingletter@gmail.com');
            await client.waitUntil(async () => {
                const accountValue = await accountInput.getValue();
                return accountValue === 'codingletter@gmail.com';
            }, 5000, '輸入帳號失敗');
            // 輸入密碼
            const passwordInput = await client.$('input[placeholder="Password"]');
            await passwordInput.click();
            await passwordInput.setValue('12344321');
            await client.waitUntil(async () => {
                const passwordValue = await passwordInput.getValue();
                return passwordValue === '12344321';
            }, 5000, '輸入密碼失敗');
            // 點擊登入按鈕
            const loginBtn = await client.$('//form//button[contains(., "登入")]');
            await loginBtn.waitForClickable();
            await loginBtn.click();
            // 確認是否登入成功
            await client.waitUntil(async () =>
                await client.getUrl() === 'https://buy.yachsp.com/',
                {timeout: 10000}
            );
            // 將鼠標移動到人像處
            const avatar = await client.$('//*[@id="__nuxt"]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/div[1]');
            await avatar.moveTo();
            // 找尋登入帳號名稱
            const accountName = await client.$('//*[@id="el-id-1024-10"]/div/div[1]/div/div');
            await accountName.waitForDisplayed();
            // 斷言
            assert.ok((await accountName.getText()).includes('letter'));
        });
    });

    describe('3.登出功能測試', function () {
        it('(1)登入後，可在會員頭像處選擇登出', async function () {
            // 將鼠標移動到人像處
            const avatar = await client.$('//*[@id="__nuxt"]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/div[1]');
            await avatar.moveTo();
            // 點擊登出按鈕
            const logoutBtn = await client.$('//*[@id="el-id-1024-10"]/div/div[1]/div/div/div[2]/div/button');
            await logoutBtn.waitForClickable();
            await logoutBtn.click();
            // 確認是否登出成功
            await client.waitUntil(async () =>
                await client.getUrl() === 'https://buy.yachsp.com/',
                {timeout: 10000}
            );
            // 將鼠標移動到人像處
            const avatar2 = await client.$('//*[@id="__nuxt"]/div/div[1]/div[1]/div/div[1]/div/div/div[2]/div[1]');
            await avatar2.moveTo();
            // 找尋登入按鈕
            const loginBtn = await client.$('#login');
            await loginBtn.waitForDisplayed();
            // 斷言
            assert.ok((await loginBtn.getText()).includes('登入/註冊'));
        });
    });

    // 在測試案例之後執行
    after(async function () {
        // 關閉客戶端
        await client.deleteSession();
    });

});