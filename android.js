// 初始化 WebDriverIO
const {remote} = require('webdriverio');

// 設定 Appium Server 與 Appium Client 連線資訊
const capabilities = {
    platformName: 'Android',
    'appium:automationName': 'UiAutomator2',
    'appium:deviceName': 'Android',
    'appium:appPackage': 'com.android.settings',
    'appium:appActivity': '.Settings',
};

// 設定 Appium Server 與 WebDriverIO 連線資訊
const wdOpts = {
    host: process.env.APPIUM_HOST || 'localhost',
    port: parseInt(process.env.APPIUM_PORT, 10) || 4723,
    logLevel: 'info',
    capabilities,
    newCommandTimeout: 300,
};

// 測試案例
// 啟動 UIAutomator 並點擊電池資訊
async function runTest() {
    const driver = await remote(wdOpts);
    try {
        const batteryItem = await driver.$('//*[@text="Battery"]');
        await batteryItem.click();
    } finally {
        await driver.pause(1000);
        await driver.deleteSession();
    }
}

runTest().catch(console.error);