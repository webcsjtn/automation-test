<h3>網站自動化測試簡易範例</h3>
<ul>
    <li>需求套件</li>
    <ul>
        <li>node.js https://nodejs.org/en</li>
        <li>npm https://www.npmjs.com/</li>
        <li>Appium https://appium.io/docs/en/2.0/quickstart/install/</li>
        <ul>
            <li>WebdriverIO https://webdriver.io/</li>
            <li>UIAutomator</li>
            <li>Chromium</li>
        </ul>
        <li>Mocha https://mochajs.org/</li>
        <li>Android Studio</li>
    </ul>
    <li>網頁測試</li>
    <ul>
        <li>啟動Appium</li>
        <ul>
            <li>appium</li>
        </ul>
        <li>執行測試</li>
        <ul>
            <li>npm test</li>
        </ul>
    </ul>
    <li>手機測試</li>
    <ul>
        <li>啟動Appium</li>
        <ul>
            <li>appium</li>
        </ul>
        <li>執行測試</li>
        <ul>
            <li>啟動虛擬機或實體機</li>
            <li>安裝須測試APP</li>
            <li>node android.js</li>
        </ul>
    </ul>
</ul>